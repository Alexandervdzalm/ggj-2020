﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeetPredictorLocationMover : MonoBehaviour
{
    public Rigidbody2D Rigidbody2D;
    public CharacterMovementState state;
    public FootLocationPredictor LeftPredictor, RightPredictor;
    public Transform LeftNeutral, RightNeutral;

    private Rigidbody2D rb => Rigidbody2D;
    private Vector2 vel => rb.velocity;

    void FixedUpdate()
    {
        if (!state.Grounded && state.AirbornTime > 0.2f)
        {
            CastRayFor(LeftNeutral, LeftPredictor);
            CastRayFor(RightNeutral, RightPredictor);
        }
    }

    public Vector2 start, dir;
    public float length;

    private void CastRayFor(Transform neutral, FootLocationPredictor predictor)
    {
        var dir = CalculateLandingDirection(neutral.position, out var start, out var end, out var length);
        this.start = start;
        this.dir = dir.normalized;
        this.length = length;
        RaycastHit2D hit = Physics2D.Raycast(start, dir, length, layerMask: SettingsHolder.Instance.Settings.IkColliderLayerMask);
        if (hit.collider != null)
        {
            predictor.transform.position = hit.point;
        }
    }

    void OnDrawGizmos()
    {
        if(Rigidbody2D == null) return;

        bool falling = !state.Grounded && state.Falling;
        Gizmos.color = falling ? Color.green : Color.gray;
        DrawDirLine(LeftNeutral);
        DrawDirLine(RightNeutral);
    }

    private void DrawDirLine(Transform Neutral)
    {
        var dir = CalculateLandingDirection(Neutral.position, out var start, out var end, out var length);
        Gizmos.DrawLine(start, end);
    }

    private Vector2 CalculateLandingDirection(Vector2 neutral, out Vector2 start, out Vector2 end, out float length)
    {
        Vector2 dir = CalculateLandingVector(out length);
        Vector2 offset = -dir.normalized * 1f;
        start = neutral + offset;
        end = neutral + dir * length + offset;
        return dir;
    }

    private Vector2 CalculateLandingVector(out float length)
    {
        Vector2 dir = Vector2.down;
        length = (vel.magnitude + 1.5f);
        return dir;
    }
}