﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CoroutineLibrary
{
    public static IEnumerator LerpProgressCoroutine(float endTime, Action<float> action)
    {
        float elapsedTime = 0;
        while (elapsedTime < endTime)
        {
            action.Invoke(elapsedTime / endTime);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }
}
