using System;
using UnityEngine;

public static class CircleMath
{
    public static Vector2 PosOnParametricCircle(Vector2 center, float radius, float angle)
    {
        return center + new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * radius;
    }

    public static Vector2 ProjectCenterCircle(Vector2 currentPos, Vector2 direction, float radius)
    {
        return currentPos + direction.normalized * radius;
    }

    public static void CircleToLines(
        Vector2 centerCircle,
        float radius,
        bool startRight,
        int linesPerCircle,
        Action<Vector2, Vector2, int> lineAction)
    {
        float angleSteps = Mathf.PI * 2 / linesPerCircle;
        float currentAngle = 0;
        if (startRight)
        {
            currentAngle = Mathf.PI;
            angleSteps = -angleSteps;
        }

        for (int i = 0; i < linesPerCircle; i++)
        {
            Vector2 from = CircleMath.PosOnParametricCircle(centerCircle, radius, currentAngle);
            currentAngle += angleSteps;
            Vector2 to = CircleMath.PosOnParametricCircle(centerCircle, radius, currentAngle);
            lineAction.Invoke(from, to, i);
        }
    }
}
