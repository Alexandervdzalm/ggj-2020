using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    InputActions inputActions;
    private MovementController movementController;
    private JumpController jumpController;
    private Respawn respawn;

    // Start is called before the first frame update
    void Awake()
    {
        inputActions = new InputActions();

        movementController = GetComponent<MovementController>();
        if (movementController == null)
            gameObject.AddComponent<MovementController>();

        // Split into multiple classes?

        jumpController = GetComponent<JumpController>();
        if (jumpController == null)
            gameObject.AddComponent<JumpController>();
        respawn = GetComponent<Respawn>();

        // Change to events?

        inputActions.Playeractions.Move.performed += ctx => movementController.SetInput(ctx.ReadValue<Vector2>().x);
        inputActions.Playeractions.Jump.started += ctx => jumpController.StartJump();
        inputActions.Playeractions.Jump.canceled += ctx => jumpController.EndJump();
        inputActions.Playeractions.Respawn.performed += ctx => respawn.RespawnPlayer();
    }

    private void OnEnable()
    {
        inputActions.Enable();
    }

    private void OnDisable()
    {
        inputActions.Disable();
    }
}