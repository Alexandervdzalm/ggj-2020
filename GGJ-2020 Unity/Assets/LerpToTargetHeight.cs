﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpToTargetHeight : MonoBehaviour
{
    [Range(0f, 2f)] public float TargetHeight = 1.0f;
    [Range(0f, 1f)] public float DistanceLerpActivatesAboveTargetHeight = 0.2f;

    public Transform LeftFootTransform, RightFootTransform;
    private Rigidbody2D rb;
    private Vector2 pos => rb.position;
    private Vector2 posLeft => LeftFootTransform.position;
    private Vector2 posRight => RightFootTransform.position;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    public float LeftPos, RightPos, TorsoPos, NewHeight;

    public float baseHeight, targetHeight;

    public bool leftIsLowest;

    // Update is called once per frame
    void FixedUpdate()
    {
        LeftPos = posLeft.y;
        RightPos = posRight.y;
        TorsoPos = pos.y;
        leftIsLowest = posLeft.y < posRight.y;
         baseHeight = leftIsLowest ? posLeft.y : posRight.y;
         targetHeight = baseHeight + TargetHeight;
        if (pos.y > baseHeight + DistanceLerpActivatesAboveTargetHeight)
            return;

        float newHeight = Mathf.Lerp(pos.y, targetHeight, 0.5f); // add condition if close?
        Vector2 targetPos = new Vector2(pos.x, newHeight);
        rb.MovePosition(targetPos);

        NewHeight = newHeight;
    }

    void OnDrawGizmos()
    {
        // if (hit.collider != null)
        // {
        //     Gizmos.color = Color.red;
        //     Gizmos.DrawLine(transform.position, hit.centroid);
        //     Gizmos.DrawWireCube(hit.centroid, Vector3.one * 0.1f);
        // }
        // else
        // {
        //     Gizmos.color = Color.green;
        //     Gizmos.DrawLine(transform.position, (Vector2) transform.position + Vector2.down * TargetHeight);
        // }
    }
}