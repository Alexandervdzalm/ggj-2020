﻿using System.Collections;
using System.Collections.Generic;
using Framework;
using UnityEngine;

public class GroundedFootIk : MonoBehaviour
{
    public List<Collider2D> FootColliders;
    public int Colliding;
    private CharacterMovementState state;
    private Settings settings => SettingsHolder.Instance.Settings;

    void Start()
    {
        state = GetComponent<CharacterMovementState>();
    }

    void FixedUpdate()
    {
        state.PreviousGrounded = state.Grounded;
        Colliding = 0;

        FootColliders.ForEach(c =>
        {
            if (c.IsTouchingLayers(settings.IkColliderLayerMask))
                Colliding++;
        });


        if (Colliding >= settings.MinimumCollidingForGrounded)
        {
            state.Grounded = true;
        }
        else if (Colliding <= settings.MinimumCollidingForNotGrounded)
        {
            state.Grounded = false;
        }

        if (!state.PreviousGrounded && state.Grounded)
        {
            Events.Instance.Raise(new LandEvent());
        }
    }
}

public class LandEvent : GameEvent { }