﻿using System;
using System.Collections.Generic;
using EasyButtons;
using UnityEditor;
using UnityEngine;


[ExecuteInEditMode]
public class FootLocationPredictor : MonoBehaviour
{
    public List<Vector2> BackwardFeetPosition = new List<Vector2>();
    public List<Vector2> ForwardFeetPosition = new List<Vector2>();

    public Transform Target;
    public float Radius => settings.StepRadius;

    private Settings settings;

    void Start()
    {
        settings = SettingsHolder.Instance.Settings;
    }

    [Button]
    void Update()
    {
        Predict();
    }

    public void Predict()
    {
        if (settings == null) settings = SettingsHolder.Instance.Settings;

        var pos = Target.position;
        PredictFootPositions(BackwardFeetPosition, false, pos);
        PredictFootPositions(ForwardFeetPosition, true, pos);
    }

    public float CalculateFootProgress(float xProgress)
    {
        var list = getRightList(xProgress);
        float xRelative = list[0].x + xProgress;
        for (int i = 0; i < list.Count - 1; i++)
        {
            float x1 = list[i].x;
            float x2 = list[i + 1].x;
            float min = Mathf.Min(x1, x2);
            float max = Mathf.Max(x1, x2);
            bool largerThanMin = xRelative - min >= 0;
            bool smallerThanMax = max - xRelative >= 0;
            if (largerThanMin && smallerThanMax)
            {
                float progress = (xRelative - x1) / (x2 - x1);
                return Mathf.Sign(xRelative) * (i + progress);
            }
        }

        return list.Count - 1;
    }

    public void UpdateTargetPos(float totalProgress, float xProgress)
    {
        var list = getRightList(xProgress);
        var abs = Mathf.Abs(totalProgress);
        if (abs >= list.Count - 2)
        {
            int indexToSet = Mathf.FloorToInt(abs);
            Target.position = list[indexToSet];
            Predict();
        }
    }

    private void PredictFootPositions(List<Vector2> list, bool right, Vector2 startPos)
    {
        list.Clear();
        Vector2 currentPos = startPos;
        list.Add(startPos);
        for (int i = 0; i < settings.CalculatePreSteps; i++)
        {
            Vector2 nextPos = RayCastToNextPosition(currentPos, right, out bool successFullHit);
            if (!successFullHit)
                break;
            if (nextPos == currentPos)
                break;
            list.Add(nextPos);
            currentPos = nextPos;
        }
    }

    private Vector2 RayCastToNextPosition(Vector2 currentPos, bool right, out bool successFullHit)
    {
        Vector2 centerCircle = CircleMath.ProjectCenterCircle(
            currentPos,
            right ? Vector2.right : Vector2.left,
            Radius);

        bool foundFeetPos = false;
        Vector2 newFeetPos = currentPos;
        CircleMath.CircleToLines(
            centerCircle,
            Radius,
            right,
            settings.RaysPerCircle,
            (Vector2 from, Vector2 to, int index) =>
            {
                if (foundFeetPos)
                    return;
                if (index == 0)
                    return;

                Vector2 direction = (to - from);
                float distance = Mathf.Abs(direction.magnitude);
                var hit = Physics2D.Raycast(from, direction.normalized, distance: distance,
                    layerMask: settings.IkColliderLayerMask);
                if (hit.collider != null)
                {
                    float dotAngle = Vector2.Dot(hit.normal, Vector2.up);
                    if (dotAngle < settings.maxAllowedDotNormalAngleForFeet)
                        return;

                    foundFeetPos = true;
                    newFeetPos = hit.point;
                }
            });
        successFullHit = foundFeetPos;
        return newFeetPos;
    }

    private List<Vector2> getRightList(float xProgress)
    {
        return xProgress < 0 ? BackwardFeetPosition : ForwardFeetPosition;
    }

    #region Gizmos

    void OnDrawGizmos()
    {
        DrawCircles(Color.blue, true, ForwardFeetPosition);
        DrawCircles(Color.red, false, BackwardFeetPosition);
    }

    private void DrawCircles(Color color, bool right, List<Vector2> list)
    {
        Gizmos.color = color;
        for (int i = 0; i < list.Count; i++)
        {
            var pos = list[i];
            var notLastIndex = i < list.Count - 1;
            if (notLastIndex)
                DrawCircleWithLines(pos, right);
            Gizmos.DrawWireCube(pos, Vector3.one * 0.1f);
        }
    }

    private void DrawCircleWithLines(Vector2 pos, bool right)
    {
        // Direction is x product of impact normal
        // Make sure start and end angles are based on that direction vector
        Vector2 direction = right ? Vector2.right : Vector2.left;
        Vector2 center = CircleMath.ProjectCenterCircle(pos, direction, Radius);
        Gizmos.DrawWireSphere(center, Radius);
        CircleMath.CircleToLines(center, Radius, right, settings.RaysPerCircle, (Vector2 from, Vector2 to, int index) =>
        {
            Gizmos.DrawLine(from, to);
#if UNITY_EDITOR
            Handles.Label(from, $"[{index}]");
#endif
        });
    }

    #endregion
}