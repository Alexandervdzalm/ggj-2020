﻿using System.Collections;
using System.Collections.Generic;
using Framework;
using UnityEngine;

public class SlamJelly : GameEvent
{
    public Vector2 ImpactPos;
    public float Impact;

    public SlamJelly(Vector2 impactPos, float impact)
    {
        ImpactPos = impactPos;
        Impact = impact;
    }
}
