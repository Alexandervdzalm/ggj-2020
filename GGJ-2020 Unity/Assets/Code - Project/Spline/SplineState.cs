﻿using System.Collections;
using System.Collections.Generic;
using EasyButtons;
using UnityEditor;
using UnityEngine;
using UnityEngine.U2D;

[ExecuteInEditMode]
public class SplineState : MonoBehaviour
{
    public List<SplinePoint> SplinePoints = new List<SplinePoint>();
    private SpriteShapeController shape;
    private Spline spline => shape.spline;

    public void Awake()
    {
        if (shape == null) shape = GetComponent<SpriteShapeController>();
        SyncSplinePointsCountWithSplineCount();
    }

    [Button]
    public void Clear()
    {
        SplinePoints.Clear();
    }

    [Button]
    public void ResetVelocity()
    {
        SplinePoints.ForEach(p => p.Velocity = Vector2.zero);
    }

    [Button]
    public void SyncSplinePointsCountWithSplineCount()
    {
        var pointCount = spline.GetPointCount();

        if (SplinePoints.Count == pointCount)
            return;

        // Setup all splinePoints
        SplinePoints.Clear();
        for (int i = 0; i < pointCount; i++)
        {
            SplinePoints.Add(new SplinePoint(spline.GetPosition(i), i));
        }
    }

    void OnDrawGizmos()
    {
        Vector2 pos = transform.position;
        SplinePoints.ForEach(p =>
        {
            Gizmos.DrawCube(pos + p.Anchor,Vector3.one * 0.15f);
            Gizmos.DrawLine(pos + p.Current, pos + p.Current + p.Velocity);
            Gizmos.DrawSphere(pos + p.Current, 0.2f);
#if UNITY_EDITOR
            Handles.Label(pos + p.Current + Vector2.down * 0.05f, p.Index.ToString());
            Handles.Label(pos + p.Anchor + Vector2.down * 0.05f, p.Index.ToString());
#endif
        });
    }
}