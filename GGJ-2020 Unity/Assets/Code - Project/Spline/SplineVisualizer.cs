﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection.Emit;
using UnityEditor;
using UnityEngine;
using UnityEngine;
using UnityEngine.U2D;

public class SplineVisualizer : MonoBehaviour
{
    public bool DrawTangents = true;
    public float PosRadius = 0.2f;
    public Color PosColor = Color.blue;
    public float TangentRadius = 0.1f;
    public Color TangentLeftColor = Color.red;
    public Color TangentRightColor = Color.green;
    private SpriteShapeController shape;
    private Spline spline => shape.spline;
    

    void Start()
    {
        this.shape = GetComponent<SpriteShapeController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnDrawGizmos()
    {
        if (shape == null) Start();

        var oldColor = Gizmos.color;
        int pointCount = spline.GetPointCount();

        for (int i = 0; i < pointCount; i++)
        {
            var center = spline.GetPosition(i) + transform.position;
            var right = spline.GetRightTangent(i) + center;
            var left = spline.GetLeftTangent(i) + center;
            Gizmos.color = PosColor;
            Gizmos.DrawWireSphere(center, PosRadius);

            #if UNITY_EDITOR
            Handles.Label(center, $"[{i}]");
            #endif

            if (!DrawTangents) continue;
            Gizmos.color = TangentRightColor;
            Gizmos.DrawWireSphere(right, TangentRadius);
            Gizmos.color = TangentLeftColor;
            Gizmos.DrawWireSphere(left, TangentRadius);
        }

        Gizmos.color = oldColor;
    }
}
