﻿using UnityEngine;

[ExecuteInEditMode]
public class ObjectPoolCollection : SingletonMonoBehavior<ObjectPoolCollection>
{
    [SerializeField]
    private ScriptableObjectObjectPool _objectPool;

    public ObjectPool ParticlesPool => _objectPool["Particles"];
    public ObjectPool SplineNodePool => _objectPool["SplineNode"];

    void Awake()
    {
        _objectPool.Init();
    }
}
