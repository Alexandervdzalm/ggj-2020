﻿using System.Collections;
using System.Collections.Generic;
using EasyButtons;
using Framework;
using UnityEngine;

public class GameStateManager : MonoBehaviour
{
    public Settings Settings;

    void Start()
    {
        Events.Instance.AddListener<GameStateTransition>(HandlePreparationForNextStateTransition);
        Events.Instance.Raise(new ReadyForTransition(GameState.Night));
    }

    private void HandlePreparationForNextStateTransition(GameStateTransition e)
    {
        Debug.Log($"GameStateManager - Received transition Change:  {e.TransitionToState}");

        // When its day we are currently finished
        if (e.TransitionToState == GameState.Day)
            return;

        Debug.Log($"Begin Preparations");

        StartCoroutine(WaitToRaise(e.TransitionToState));
    }

    public IEnumerator WaitToRaise(GameState state)
    {
        Debug.Log($"Begin waiting {Settings.WaitForNextStateTime}");
        yield return new WaitForSeconds(Settings.WaitForNextStateTime);
        Debug.Log($"waited {Settings.WaitForNextStateTime}");
        Events.Instance.Raise(new ReadyForTransition(GameStateHelper.NextGameState(state)));
    }
}

public class ReadyForTransition : GameEvent
{
    public GameState TransitionToState;

    public ReadyForTransition(GameState transitionToState)
    {
        this.TransitionToState = transitionToState;
    }
}

public class GameStateTransition : GameEvent
{
    public GameState TransitionToState;

    public GameStateTransition(GameState transitionToState)
    {
        this.TransitionToState = transitionToState;
    }
}

public enum GameState
{
    Night = 0,
    Dawn = 1,
    Day = 2
}

public static class GameStateHelper
{
    public static GameState NextGameState(GameState currentGameState)
    {
        switch (currentGameState)
        {
            case GameState.Night:
                return GameState.Dawn;
            default:
                return GameState.Day;
        }
    }
}