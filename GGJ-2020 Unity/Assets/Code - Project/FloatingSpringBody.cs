﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingSpringBody : MonoBehaviour
{
    public float TargetHeight = 0.75f;
    [Range(0.1f,1.0f)]
    public float Stiffness = 0.3f;
    [Range(0.1f, 1.0f)]
    public float Damping = 0.3f;

    private Rigidbody2D rb;
    private RaycastHit2D hit;
    private Settings settings;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        settings = SettingsHolder.Instance.Settings;
    }

    void FixedUpdate()
    {
        hit = Physics2D.Raycast(rb.position, Vector2.down, distance: TargetHeight * 2, layerMask:settings.IkColliderLayerMask);
        if (hit.collider != null && hit.distance < TargetHeight)
        {
            Vector2 velocity = rb.velocity;

            // Spring stuff
            var displacement = Mathf.Abs(hit.point.y - transform.position.y);
            Vector2 impulse = -Stiffness * displacement * Vector2.down - Damping * velocity;
            rb.velocity += impulse;
        }
    }

    void OnDrawGizmos()
    {
        if (hit.collider != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, hit.centroid);
            Gizmos.DrawWireCube(hit.centroid, Vector3.one * 0.1f);
        }
        else
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(transform.position, (Vector2) transform.position + Vector2.down * TargetHeight);
        }
    }
}