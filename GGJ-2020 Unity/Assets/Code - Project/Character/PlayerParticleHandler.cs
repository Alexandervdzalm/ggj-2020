﻿using System.Collections;
using System.Collections.Generic;
using Framework;
using UnityEngine;

public class PlayerParticleHandler : MonoBehaviour
{
    public int YinStored = 0;
    public Transform playerHeart;
    private Settings settings => SettingsHolder.Instance.Settings;

    // Start is called before the first frame update
    void Start()
    {
        Events.Instance.AddListener<SlamJelly>(OnJellyImpact);
    }

    private void OnJellyImpact(SlamJelly e)
    {
        if(e.Impact > settings.YangImpactTreshold)
            StartCoroutine(SpawnYin(e));
    }

    private IEnumerator SpawnYin(SlamJelly e)
    {
        var spawnAmount = YinStored;
        

        for (var i = 0; i < spawnAmount && YinStored > 0; i++)
        {
            var ob = ObjectPoolCollection.Instance.ParticlesPool.Spawn(playerHeart.position);
            var particleEntity = ob.GetComponent<ParticleEntity>();

            particleEntity.SetYangExplosion();

            YinStored--;

            yield return new WaitForSeconds(settings.YangExplosionTimePerParticle);
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("Particle"))
        {
            ParticleEntity entity = collider.GetComponent<ParticleEntity>();
            if (entity == null)
                return;
            if (entity.Type == ParticleType.Yang)
                return;

            entity.MoveToHeart(playerHeart, ()=> YinStored++);
        }
    }
}