﻿using System.Collections;
using System.Collections.Generic;
using Framework;
using UnityEngine;

public class CharacterMovementState : MonoBehaviour
{
    public bool Grounded = false;
    public bool PreviousGrounded = false;
    public bool Jumping = false;
    public bool Falling = false;
    public float AirbornTime = 0;
    public float GroundedTime = 0;
    public float FallingTime = 0;

    void Start()
    {
        Events.Instance.AddListener<OnJumpStart>(e => GroundedTime = 0);
    }

    void Update()
    {
        if (!Grounded)
        {
            AirbornTime += Time.deltaTime;
            GroundedTime = 0;
            Events.Instance.Raise(new OnAirborn(this));
        }
        else
        {
            AirbornTime = 0;
            GroundedTime += Time.deltaTime;
            Events.Instance.Raise(new OnGrounded(this));
        }

        if (Falling)
        {
            FallingTime += Time.deltaTime;
        }
        else
        {
            FallingTime = 0;
        }
    }
}

public class OnGrounded : GameEvent
{
    public CharacterMovementState state;

    public OnGrounded(CharacterMovementState state)
    {
        this.state = state;
    }
}
public class OnAirborn : GameEvent
{
    public CharacterMovementState state;

    public OnAirborn(CharacterMovementState state)
    {
        this.state = state;
    }
}
