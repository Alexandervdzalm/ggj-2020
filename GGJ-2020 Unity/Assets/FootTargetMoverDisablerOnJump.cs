﻿using System.Collections;
using System.Collections.Generic;
using Framework;
using UnityEngine;

public class FootTargetMoverDisablerOnJump : MonoBehaviour
{
    private FootTargetMover footTargetMover;

    public float GroundedTimeBeforeOn = 0.3f;
    public float AirbornTimeBeforeOff = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        footTargetMover = GetComponent<FootTargetMover>();
        Events.Instance.AddListener<OnGrounded>(e =>
        {
            if (e.state.GroundedTime > GroundedTimeBeforeOn && !footTargetMover.On)
            {
                footTargetMover.On = true;
                Debug.Log($"OnGrounded after {e.state.GroundedTime}");
            }
        });
        Events.Instance.AddListener<OnAirborn>(e =>
        {
            if (e.state.AirbornTime > AirbornTimeBeforeOff && footTargetMover.On)
            {
                footTargetMover.On = false;
                Debug.Log($"OnAirborn after {e.state.AirbornTime}");
            }
        });
        Events.Instance.AddListener<OnJumpStart>(e =>
        {
            footTargetMover.On = false;
            Debug.Log($"OnJumpStart");
        });
    }
}